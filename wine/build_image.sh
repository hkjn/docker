#!/bin/bash
#
# Rebuilds and pushes the wine image.
#
docker build -t hkjn/wine .
docker push hkjn/wine
