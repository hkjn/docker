#!/bin/bash
#
# This script is run as the entrypoint of the container.
#

set -eo pipefail

# Generate host keys if they don't already exist. It's recommended to
# mount this directory as a volume from the host, not to include the
# keys in the Docker image.  If the image is shared (i.e. on Docker
# hub), this means that anyone that can access the image can perform
# MitM attacks on the toolbox container.
HOST_KEY=/keys/ssh_host_rsa_key
[ -e "$HOST_KEY" ] || {
  echo "Generating $HOST_KEY.."
  ssh-keygen -b 8192 -t rsa -f "$HOST_KEY"
  chmod 400 "$HOST_KEY"
}

KEYS="/home/user/.ssh/"
[ -e "$KEYS/authorized_keys" ] || {
  echo "FATAL: No $KEYS/authorized_keys; please mount with:" >&2
  echo "docker run -v /host/path:$KEYS" >&2
  exit 1
}
[ -r "$KEYS/authorized_keys" ] || {
  echo "FATAL: Can't read $KEYS/authorized_keys." >&2
  exit 2
}

# The root user inside the container must own the keys/ directory, or
# sshd will refuse to use it.
# chown -R root:root "$KEYS/"
echo "Setting permissions.."
chmod -R 400 "/keys"
chmod -R 700 "$KEYS"
chown -R root:root "/keys/"
chown -R user:user "$KEYS"

GIT_CACHE="/home/user/.git-credential-cache"
[ -d "$GIT_CACHE" ] && {
		# It's not required to have a .git-credential-cache, but if we do
		# it's likely mounted from the host, and we need to have the right
		# permissions / ownership.
		chown -R user:user "$GIT_CACHE"
		chmod -R 700 "$GIT_CACHE"
}

exec supervisord -c /etc/supervisord.conf "$@"
