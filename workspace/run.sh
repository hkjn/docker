#!/bin/bash
#
# This script starts a workspace container with some default options.
#

set -eo pipefail

[ -d "$HOME/.workspace/chost" ] || {
		echo "FATAL: No host keys for workspace container; checked $HOME/.workspace/chost/." >&2
		exit 1
}
[ -d "$HOME/.workspace/csh" ] || {
		echo "FATAL: No authorized_keys directory present; checked $HOME/.workspace/csh/" >&2
		exit 2
}

docker run -d --name $USER-workspace \
  -p 8098:22 \
  -v "$HOME/.workspace/chost:/keys" \
  -v "$HOME/.workspace/csh:/home/user/.ssh" \
  -v "$HOME/.workspace/gitcreds:/home/user/.git-credential-cache" \
  -v "$HOME/src:/home/user/src" \
  hkjn/workspace
