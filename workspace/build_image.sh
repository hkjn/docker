#!/bin/bash
#
# Rebuilds the Docker image.
#
docker build -t hkjn/workspace . && docker push hkjn/workspace
