#!/bin/bash
#
# This script is run as the entrypoint of the container.
#

set -eo pipefail

if [ ! -e "/keys/authorized_keys" ]; then
		echo "FATAL: No authorized_keys file in /keys; please mount with:" >&2
		echo "docker run -v /host/path:/keys" >&2
		exit 1
fi

# The owner of the keys/ directory must be root user inside the
# container, or sshd will refuse to use it (but remain silent about
# this, unless it's started as /usr/sbin/sshd -D -d).
chown -R root:root /keys/
chmod -R 400 /keys/

# Generate host keys. It's recommended to mount this directory as a
# volume from the host, not to include the keys in the Docker image.
# If the image is shared (i.e. on Docker hub), this means that anyone
# that can access the image can perform MitM attacks on the toolbox
# container.
if [ ! -e "/keys/ssh_host_rsa_key" ]; then
		echo "Generating /keys/ssh_host_rsa_key.."
		ssh-keygen -b 8192 -t rsa -f /keys/ssh_host_rsa_key
		chmod 400 /keys/ssh_host_rsa_key
fi

echo "Starting supervisord.."
exec supervisord -c /etc/supervisord.conf "$@"
