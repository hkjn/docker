#!/bin/bash
#
# Rebuilds the Docker image.
#
docker build -t hkjn/toolbox . && docker push hkjn/toolbox
