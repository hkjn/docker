#
# Dockerfile for toolbox image.
#
# Create a volume from the host path for the public key(s) that should
# be allowed to SSH into the container by specifying a volume at run
# time:
# $ docker run -v /host/path:/keys
#
# The /host/path directory should contain an authorized_keys file, and
# will also be used to store the container's host keys.
#

FROM fedora

MAINTAINER Henrik Jonsson <me@hkjn.me>

RUN dnf -y update && \
  dnf -y install less emacs tmux nmap openssh-server openssh-clients supervisor iputils fuse-sshfs nano git

# Create ssh directories.
RUN mkdir /root/.ssh
RUN mkdir /var/run/sshd

# Add the sshd_config.
COPY sshd_config /etc/ssh/sshd_config

# Enable networking.
RUN echo 'NETWORKING=yes' >> /etc/sysconfig/network

# Make supervisord run in foreground.
RUN sed -i -e "s/^nodaemon=false/nodaemon=true/" /etc/supervisord.conf

# Tell supervisord to monitor sshd.
RUN echo [program:sshd] >> /etc/supervisord.d/ssh.ini
RUN echo 'command=/usr/sbin/sshd -D' >> /etc/supervisord.d/ssh.ini
RUN echo  >> /etc/supervisord.d/ssh.ini

# Expose SSH port inside the container.
EXPOSE 22

# Add start script.
COPY start.sh /usr/bin/

CMD ["start.sh"]
